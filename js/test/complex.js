/**
 * 
 */

ComplexNumbersTest = TestCase("ComplexNumbersTest");

ComplexNumbersTest.prototype.setUp = function() {
	this.comp1 = new Complex(1,3);
	this.comp2 = new Complex(2,2);
};

ComplexNumbersTest.prototype.testEquals = function(){
	var c1 = new Complex(1,2);
	var c2 = new Complex(1,2);
	assertTrue("Integer complex values should be equal.", c1.equals(c2));
	
	c1 = new Complex(1.2, 4.0);
	c2 = new Complex(1.2, 4.0);
	assertTrue("Float complex values should be equal.", c1.equals(c2));
	
	c1 = new Complex(1.0, 4);
	c2 = new Complex(1, 4.0);
	assertTrue("Mixex float and integer complex values should be equal.", c1.equals(c2));
	
	c1 = new Complex(1.5, 4.0);
	c2 = new Complex(1.2, 4);
	assertFalse("Mixed complex values should not be equal.", c1.equals(c2));
};

ComplexNumbersTest.prototype.testAdd = function(){
	var c = this.comp1.add(this.comp2);
	assertEquals("ADD: real number should give 3 instead of " + c.a, 3, c.a);
	assertEquals("ADD: imag number should give 5 instead of " + c.j, 5, c.j);
};

ComplexNumbersTest.prototype.testSub = function(){
	var c = this.comp1.sub(this.comp2);
	assertEquals("SUB: real number should give -1 instead of " + c.a, -1, c.a);
	assertEquals("SUB: imag number should give 1 instead of " + c.j, 1, c.j);
};

ComplexNumbersTest.prototype.testMul = function(){
	var c = this.comp1.mul(this.comp2);
	assertEquals("MUL: real number should give -4 instead of " + c.a, -4, c.a);
	assertEquals("MUL: imag number should give 1 instead of " + c.j, 8, c.j);
};

ComplexNumbersTest.prototype.testAbs = function(){
	var c = this.comp2.abs();
	assertEquals("MUL: real number should give 2 instead of " + c.a, Math.sqrt(8.0), c.a);
	assertEquals("MUL: imag number should give 0 instead of " + c.j, 0, c.j);
};

ComplexNumbersTest.prototype.testPow = function(){
	assertTrue("POW: power 1 failed. ", this.comp1.equals(this.comp1.pow(1)));
	assertTrue("POW: power 1 failed. ", this.comp2.equals(this.comp2.pow(1)));
	
	var c1 = this.comp1.mul(this.comp1);
	var c2 = this.comp2.mul(this.comp2);
	assertTrue("POW: power 2 failed. ", c1.equals(this.comp1.pow(2)));
	assertTrue("POW: power 2 failed. ", c2.equals(this.comp2.pow(2)));
	
	c1 = c1.mul(this.comp1);
	c2 = c2.mul(this.comp2);
	assertTrue("POW: power 3 failed. ", c1.equals(this.comp1.pow(3)));
	assertTrue("POW: power 3 failed. ", c2.equals(this.comp2.pow(3)));
	
	c1 = c1.mul(this.comp1);
    c2 = c2.mul(this.comp2);
	assertTrue("POW: power 4 failed. ", c1.equals(this.comp1.pow(4)));
	assertTrue("POW: power 4 failed. ", c2.equals(this.comp2.pow(4)));
};
