function Gradient(canvas){
    this.canvas = canvas;
    this.context = canvas.getContext('2d');
    
    this.width = canvas.width;
    this.height = canvas.height;
    this.gradient = this.context.createLinearGradient(0,0,this.width,0);
    this.stops = {};
    this.colorTable = [];
}

Gradient.prototype.recompute = function(){
    this.clear();
    this.update();
    this.draw();
    this.updateColorTable();
    console.log();
}

Gradient.prototype.updateWidth = function(width){
    this.width = width;
    this.recompute();
}

Gradient.prototype.updateColorTable = function(){
    var colors = this.getColor(0,0, this.width);
    for(var i=0; i<colors.length; i+=4){
        this.colorTable[i/4] = [colors[i], colors[i+1], colors[i+2], colors[i+3]];
    }
}

Gradient.prototype.addColorStop = function(percent, color){
    this.gradient.addColorStop(percent, color);
    this.stops[percent] = color;
}

Gradient.prototype.removeColorStop = function(color){
    var percent = false;
    for(per in this.stops){
        if ( color == this.stops[per]){
            percent = per;
            break;
        }
    }
    if ( percent ) {
        delete this.stops[percent];
        this.update();
        return true;
    }
    return false;
}

Gradient.prototype.draw = function(){
    this.context.fillStyle = this.gradient;
    this.context.fillRect(0,0, this.width, this.height);
}

Gradient.prototype.clear = function(){
    this.context.clearRect(0,0,this.width, this.height);
}

Gradient.prototype.update = function(){
    this.gradient = this.context.createLinearGradient(0,0, this.width, 0);
    for(percent in this.stops){
        this.addColorStop(percent, this.stops[percent]);
    }
}

Gradient.prototype.getColorInHex = function(position){
    var data = this.getColor(position);
    var color = this.convertDataToHexColor(data);
//     var hex = this.rgbToHex(data[0], data[1], data[2]);
//     hex = "#" + ("000000" + hex);
    return color;
}

Gradient.prototype.convertDataToHexColor = function(data){
    var color = this.rgbToHex(data[0], data[1], data[2]);
    color = "#" + ("000000" + color);
    return color;
}

Gradient.prototype.getColor = function(x, y, len){
    y = y || this.height - 1;
    len = len || 1;
    var data = this.context.getImageData(x,y, len,1).data;
    return data;
}

// 100% = 1
// 0% = 0, range is from 0 to 1
Gradient.prototype.convertPercentToPosition = function(percent){
    if ( percent > 1 ) percent = 1;
    if ( percent < 0 ) percent = 0;
    return parseInt( percent * (this.width-1) );
}

Gradient.prototype.convertPositionToPercent = function(position){
    if ( position > this.width || position < 0 ) throw "Invalid position";
    return parseFloat( position / this.width );
}

Gradient.prototype.rgbToHex = function(r, g, b) {
    if (r > 255 || g > 255 || b > 255)
        throw "Invalid color component";
    return ((r << 16) | (g << 8) | b).toString(16);
}

Gradient.prototype.drawIntervals = function(){
    var ctx = this.context;
    ctx.strokeStyle = "#000";
    ctx.lineWidth = ~~(this.width * 0.01);
    ctx.beginPath();
    for(var stop in this.stops){
        var position = this.convertPercentToPosition(stop);
        ctx.moveTo(position, 0);
        ctx.lineTo(position, ~~(this.height/2));
    }
    ctx.stroke();
}

