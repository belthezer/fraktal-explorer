function canvasResize(){
    var canvas = $("#canvas-fractal");
    var selection =$("#canvas-selection");
    var gradient = $("#canvas-gradient");
    
    w = $(window).width();
    h = $(window).height();

    canvas.attr("width", w).attr("height", h);
    selection.attr("width", w).attr("height", h);
    gradient.attr("width", w);
    
    if ( canvasEngine )
    {
        canvasEngine.clear();
        canvasEngine.drawFractal();
        canvasEngine.gradient.updateWidth(w);
    };
};

$(document).ready(function(){
    $(window).resize(canvasResize);
    canvasResize();
    
    var canvas = document.getElementById("canvas-fractal");
    var canvasGradient = document.getElementById("canvas-gradient");
    var canvasColorPicker = document.getElementById("canvas-color-picker");
    
    canvasEngine = new CanvasEngine(canvas);
    var gradient = new Gradient(canvasGradient);
    gradient.addColorStop(0, '#000');
    gradient.addColorStop(1, '#fff');
    gradient.addColorStop(1/5, '#aff');
    gradient.addColorStop(3/5, '#af0');
    gradient.recompute();
    gradient.drawIntervals();
    canvasEngine.gradient = gradient;
    
    // drawing all color on canvas color picker
    var colorPicker = new Gradient(canvasColorPicker);
    colorPicker.addColorStop(0,    "rgb(255,   0,   0)");
    colorPicker.addColorStop(0.15, "rgb(255,   0, 255)");
    colorPicker.addColorStop(0.33, "rgb(0,     0, 255)");
    colorPicker.addColorStop(0.49, "rgb(0,   255, 255)");
    colorPicker.addColorStop(0.67, "rgb(0,   255,   0)");
    colorPicker.addColorStop(0.84, "rgb(255, 255,   0)");
    colorPicker.addColorStop(1,    "rgb(255,   0,   0)");
    colorPicker.recompute();

    // Create semi transparent gradient (white -> trans. -> black)
    colorPicker.gradient = colorPicker.context.createLinearGradient(0, 0, 0, colorPicker.height);
    colorPicker.addColorStop(0,   "rgba(255, 255, 255, 1)");
    colorPicker.addColorStop(0.5, "rgba(255, 255, 255, 0)");
    colorPicker.addColorStop(0.5, "rgba(0,     0,   0, 0)");
    colorPicker.addColorStop(1,   "rgba(0,     0,   0, 1)");
    colorPicker.draw();
    
    
    // drawing fractal
    canvasEngine.drawFractal();
    canvasEngine.updatePanel();
    ////////////////////////////////////////////////////////////////////////////////
    // controlers from panel
    
    // initiate startup values
    $('#julia-coordinates-keepratio').val( canvasEngine.keepRatio || 1 );
    $('#julia-performance-maxloop').val( canvasEngine.maxLoop.toString() || 255 );
    $('#julia-performance-maxFractalWorkers').val( canvasEngine.maxFractalWorkers.toString() || 4 );
    $('#julia-performance-maxvaluepoint').val( canvasEngine.maxPointValue.toString() || 2.0 );
    $('#julia-fractal-point-a').val( 0.38 );
    $('#julia-fractal-point-j').val( -0.25 );
    
    // initiate events
    $('#julia-coordinates-keepratio').change(function(){
        var val = canvasEngine.keepRatio;
        canvasEngine.keepRatio = $(this).is(':checked') ? 1 : 0;
    }).change();
    
    $('#julia-view-progressbars').change(function(){
        if ( $(this).is(':checked') ) {
            $('#compute-progressbar').css('display', 'block');
            $('#draw-progressbar').css('display', 'block');
        }else {
            $('#compute-progressbar').css('display', 'none');
            $('#draw-progressbar').css('display', 'none');
        }
        
    }).change();
    
    $('#julia-fractal-point-a').change(function(){
        if ( canvasEngine.fractal && canvasEngine.fractal.c ){
            var c = canvasEngine.fractal.c;
            c.a = $(this).val();
            canvasEngine.fractal.setC(c);
        };
    }).change();
    
    $('#julia-fractal-point-j').change(function(){
        if ( canvasEngine.fractal && canvasEngine.fractal.c ){
            var c = canvasEngine.fractal.c;
            c.j = $(this).val();
            console.log('changed j:', c.j);
            canvasEngine.fractal.setC(c);
        };
    }).change();
    
    // section performance in control panel
    $('#julia-performance-maxloop').change(function(){
        var val = canvasEngine.maxLoop;
        canvasEngine.maxLoop = parseInt($(this).val()) || val;
        canvasEngine.fractal.getValuesFromPanel();
    }).change();

    $('#julia-performance-maxFractalWorkers').change(function(){
        var val = canvasEngine.maxFractalWorkers;
        canvasEngine.maxFractalWorkers = parseInt($(this).val());
        canvasEngine.fractal.getValuesFromPanel();
    }).change();
    
    $('#julia-performance-maxvaluepoint').change(function(){
        var val = canvasEngine.maxPointValue;
        canvasEngine.maxPointValue = parseFloat($(this).val()) || val;
        canvasEngine.fractal.getValuesFromPanel();
    }).change();
    
    $('#julia-fractal-equationtype').change(function() {
        var value = $('#julia-fractal-equationtype option:selected').val();
        var fractal = false;
        if ( value == 'Mandelbrot' ){
            fractal = new Mandelbrot(canvasEngine.width, canvasEngine.height);
        };
        if ( value == 'Julia' ){
            fractal = new Julia(canvasEngine.width, canvasEngine.height);
        };
        if ( value == 'Plasma' ){
            fractal = new Plasma(canvasEngine.width, canvasEngine.height);
        };
        canvasEngine.bindFractal(fractal);
    });
    
    // event on buttons
    $('#control-panel-button').click(function(){
        $('#control-panel').toggle();
    });
    
//     $('#logs-button').click(function(){
//         $('#logs-panel').toggle();
//     });
    
    $('#button-refresh').click(function() {
        canvasEngine.drawFractal();
        canvasEngine.updatePanel();
    });
    
    $('#button-save-jpeg').click(function() {
        var data = document.getElementById("canvas-fractal").toDataURL("image/png");
        document.location.href = data.replace("image/png", "image/octet-stream");
    });
    
    $('#control-zoomin').click(function(){
        canvasEngine.zoomIn();
        canvasEngine.updatePanel();
        canvasEngine.drawFractal();
        
    });
    
    $('#control-zoomout').click(function(){
        canvasEngine.zoomOut();
        canvasEngine.updatePanel();
        canvasEngine.drawFractal();
    });
    
    
    
});

