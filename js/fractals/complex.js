/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Class Complex converts classical point into complex point
// so x => a
//    y => j
function Complex(x,y){
    this.a = x;
    this.j = y;
};

Complex.prototype.add = function(c){
    return new Complex(this.a+c.a, this.j+c.j);
};

Complex.prototype.sub = function(c){
    return new Complex(this.a-c.a, this.j-c.j);
};

Complex.prototype.mul = function(c){
    var tempa = this.a*c.a + this.j*c.j*(-1);
    var tempj = (this.j*c.a) + (this.a*c.j);
    return new Complex(tempa, tempj);
};

Complex.prototype.div = function(a,j){
    // TODO
};

Complex.prototype.abs = function(){
    return new Complex(Math.sqrt((this.a*this.a)+(this.j*this.j)), 0);
};

Complex.prototype.pow = function(power){
    var c = this.copy();
    var z = this.copy();
    for(var i = 1; i < power; i++) {
        z = z.mul(c);
    }
    return z;
};

Complex.prototype.inverse = function(){
    return new Complex(-this.a, -this.j);
};

Complex.prototype.equals = function(c){
    if ( c.a == this.a && c.j == this.j)
        return true;
    else{
        return false;
    }
};

Complex.prototype.copy = function(){
    return new Complex(this.a, this.j);
};
