importScripts('complex.js');
importScripts('fractal.js');
importScripts('plasma.js');

function computeRow(data, rowNumber)
{
    result = []
    complexPoint.a = data['startA'];
    complexPoint.j = data['startJ'] + (rowNumber*data['sizeJ']);
    for (var x = 0; x < data['width']; x++, complexPoint.a+=data['sizeA']) {            
        var n = fractal.computeMandelbrot(complexPoint);
        result.push(n);
    }
    return result;
}

function computeColumn(data, columnNumber)
{
    result = []
    complexPoint.a = data['startA'] + (columnNumber*data['sizeA']);
    complexPoint.j = data['startJ'];
    for (var y = 0; y < data['height']; y++, complexPoint.j+=data['sizeJ']) {            
        var n = fractal.computeMandelbrot(complexPoint);
        result.push(n);
    }
    return result;
}

onmessage = function(oEvent){
    var data = oEvent.data;
    //var fractal = data['method'];
    var c = data['points'];
    
    fractal = new Fractal();
    fractal.maxLoop = data['maxLoop'] || 255;
    fractal.maxPointValue = data['maxPointValue'] || fractal.maxPointValue;
    
    var result = {
        points:[],
        line: 0,
        'state': 'working',
        'worker': data['worker'],
        'computeMethod': data['computeMethod'],
        'keys': [],
    };
    
    range = false;
    computeFunction = false;
    computeMethod = data['computeMethod'];
    if ( computeMethod == 'byColumns' ){
        range = data['width'];
        console.log('tutaj2');
        computeFunction = computeColumn;
    }
    if ( computeMethod == 'byRows' ){
        range = data['height'];
        console.log('tutaj');
        computeFunction = computeRow;
    }
    
    complexPoint = new Complex(0,0);
    var amount = parseInt(range/data['workers']);
    
    for (var i = amount*data['worker']; i < amount*(data['worker']+1); i++){
        result['line'] = i;
        result['keys'].push(i);
        result['points'] = computeFunction(data, i);
        postMessage(result);
    }
    
    result['state'] = 'done';
    postMessage(result);
    self.close();
};