function Julia(width, height, context)
{
    ComplexFractal.call(this, width, height, context);
    this.setC(new Complex(0.45,-0.11));
}

extend(ComplexFractal, Julia, ComplexFractal.prototype);

Julia.prototype.init = function(params) {
    ComplexFractal.prototype.init.call(this, params);
    $('#julia-fractal-point').show();
    var a = $('#julia-fractal-point-a').val();
    var j = $('#julia-fractal-point-j').val();
    if ( a && j ) {
        a = parseFloat(a);
        j = parseFloat(j);
        this.setC(new Complex(a,j));
    }
};

Julia.prototype.destroy = function(params) {
    ComplexFractal.prototype.destroy.call(this, params);
    $('#julia-fractal-point').hide();
};

Julia.prototype.setC = function(c){
    this.c = c || this.c;
    return this.c;
}

Julia.prototype.reconstruct = function(params) {
    ComplexFractal.prototype.reconstruct.call(this, params);
    if ( 'cA' in params && 'cJ' in params ) {
        var a = parseFloat( params['cA'] );
        var j = parseFloat( params['cJ'] );
        this.setC(new Complex( a,j ));
    }
};

Julia.prototype.createMessage = function()
{
    var message = ComplexFractal.prototype.createMessage.call(this);
    message['fractal'] = 'julia';
    message['cA'] = this.c.a;
    message['cJ'] = this.c.j;
    return message;
}

////////////////////////////////////////////////////////////////////////
// Complex Methods

Julia.prototype.compute = function(x,y){ 
    var cpoint = this.convertToComplex(x,y);
    return this.computeComplex(cpoint, this.c);
};  

Julia.prototype.computeComplex = function(complexPoint, c){ 
    var c = c || this.c;
    var z = complexPoint.copy();
    var i = 0; 
    for(i;i<=this.maxLoop;i++)
    {
        if ( z.abs().a > this.maxPointValue ) {
            break;
        }
        // z=z^2+c
        z = z.pow(2).add(c);
    };
    return i;
};
