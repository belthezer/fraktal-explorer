/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Class Points. Stores by default x,y attrs. 
// Below some basic methods.
function Point(x,y){
    this.x = x;
    this.y = y;
};

Point.prototype.add = function(p){
    return new Point(this.x + p.x, this.y + p.y);
};

Point.prototype.sub = function(p){
    return new Point(this.x - p.x, this.y - p.y);
};

Point.prototype.equals = function(p){
    if ( p.x == this.x && p.y == this.y){
        return true;
    }
    else{
        return false;
    };
};

Point.prototype.copy = function(){
    return new Point(this.x, this.y);
};

