/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Class: Canvas 

function CanvasEngine(canvas) {
    // adding common attributes
    this.width = canvas.width;
    this.height = canvas.height;
    this.context = canvas.getContext('2d');
    this.canvas = canvas;

    // atributes for fractal
    this.bindFractal(new Mandelbrot(this.width, this.height));

    this.keepRatio = true;
    this.maxLoop = 45;
    this.maxPointValue = 2.0;
    
    this.workers = {};
    this.maxFractalWorkers = 4;
    this.gradient = false;
    
    // atributes for drawing
    this.block1x1 = this.context.createImageData(1, 1);
    this.data1x1 = this.block1x1.data;

    // helpers for progressbars
    this.computedLines = 0;
    this.drawnLines = 0;
};

CanvasEngine.prototype.resetProgressBars = function() {
    this.computedLines = 0;
    this.drawnLines = 0;
};

CanvasEngine.prototype.updateProgressBars = function() {
    var offset = 101;
    if ( this.fractal.storeMethod == COMPUTING_METHODS.ROWS){
        var computed = parseInt(this.computedLines/this.height*offset);
        var drawn = parseInt(this.drawnLines/this.height*offset);
    }
    if ( this.fractal.storeMethod == COMPUTING_METHODS.COLUMNS){
        var computed = parseInt(this.computedLines/this.width*offset);
        var drawn = parseInt(this.drawnLines/this.width*offset);
    }
    $('#compute-progressbar').val(computed);
    $('#loading-text').text(computed.toString());

    $('#draw-progressbar').val(drawn);
};

CanvasEngine.prototype.updateCoordinates = function() {
    var cstart = this.fractal.getStartPosition();
    var cend = this.fractal.getEndPosition();
    $('#julia-border-top').html(cstart.j.toExponential(EXP_PRECISION));
    $('#julia-border-left').html(cstart.a.toExponential(EXP_PRECISION));
    $('#julia-border-bottom').html(cend.j.toExponential(EXP_PRECISION));
    $('#julia-border-right').html(cend.a.toExponential(EXP_PRECISION));
}

CanvasEngine.prototype.updateZoom = function() {
    var text = '';
    var zoom = this.getZoom();
    if ( this.keepRatio ) {
        text = zoom['a'];
    }else {
        text = 'a: ' + zoom['a'] + ' j: ' + zoom['j'];
    }
    $('#julia-view-zoom').html(text);
};

CanvasEngine.prototype.updatePanel = function() {
    canvasEngine.updateZoom();
    canvasEngine.updateCoordinates();
    canvasEngine.updateProgressBars();
};

CanvasEngine.prototype.addLog = function(string) {
    var val = $('logs-textarea').val();
    $('logs-textarea').val(val+'\n'+string);
};

//////////////////////////////////////////////////////////////////////////
// Point XY methods
CanvasEngine.prototype.zoom = function(percent){
    this.fractal.zoom(percent);
};

CanvasEngine.prototype.zoomOut = function(){
    this.fractal.zoom(1.25);
};

CanvasEngine.prototype.zoomIn = function() {
    this.fractal.zoom(0.75);
};

CanvasEngine.prototype.getZoom = function(){
    return this.fractal.getZoom();
};

CanvasEngine.prototype.zoomInFromRange = function(p1, p2){
    this.fractal.zoomInFromRange(p1, p2);
};

//////////////////////////////////////////////////////////////////////////
// Drawing Methods
CanvasEngine.prototype.clear = function() {
    this.context.clearRect(0, 0, this.width, this.height);
};

CanvasEngine.prototype.putPixel = function(x, y, color) {
    var d = this.block1x1.data;
    var ctx = this.context;
    d[0] = color[0] % 256;
    d[1] = color[1] % 256;
    d[2] = color[2] % 256;
    d[3] = color[3] % 256;
    ctx.putImageData(this.block1x1, x, y);
};

CanvasEngine.prototype.putPixelRGBA = function(x, y, r, g, b, a) {
    var d = this.data1x1;
    var ctx = this.context;
    d[0] = r % 256;
    d[1] = g % 256;
    d[2] = b % 256;
    d[3] = a % 256;
    ctx.putImageData(this.block1x1, x, y);
};
////////////////////////////////////////////////////////////////////////////
// Fractal Methods
CanvasEngine.prototype.bindFractal = function(fractal) {
    if ( this.fractal ) {
        this.fractal.destroy();
    };
	this.fractal = fractal;
    this.fractal.init();
};

CanvasEngine.prototype.drawFractal = function() {
    this.fractal.draw();
};

CanvasEngine.prototype.resetWorkers = function() {
    this.workers = [];
};

CanvasEngine.prototype.getSelectedGradient = function(){
    return this.gradient;
}