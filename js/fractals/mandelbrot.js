
function Mandelbrot(width, height, context){
    ComplexFractal.call(this, width, height, context);
}

extend(ComplexFractal, Mandelbrot, ComplexFractal.prototype);

Mandelbrot.prototype.reconstruct = function(params) {
    ComplexFractal.prototype.reconstruct.call(this, params);

};

Mandelbrot.prototype.createMessage = function(){
    var message = ComplexFractal.prototype.createMessage.call(this);
    message['fractal'] = 'mandelbrot';
    return message;
}


////////////////////////////////////////////////////////////////////////
// Complex Methods

Mandelbrot.prototype.compute = function(x,y){ 
    var c = this.convertToComplex(x,y);
    return this.computeComplex(c);
};  

Mandelbrot.prototype.computeComplex = function(complexPoint){ 
	
    var c = complexPoint.copy();
    var z = complexPoint.copy();
    var i = 0; 
    for(i;i<=this.maxLoop;i++)
    {
        var a=z.abs().a;
        if ( a > this.maxPointValue || a == 0 ) {
            break;
        }
        // z=z^2+c
        z = z.pow(2).add(c);
    };
    return i;
};
